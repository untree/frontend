import { LoginPage } from './../pages/login/login.page';
import { ModalController } from '@ionic/angular';
import { AuthService } from '../services/auth/auth.service';
import { Injectable } from '@angular/core';
import { Router, CanLoad, Route, UrlSegment } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanLoad  {
  constructor(
    private router: Router,
    private authService: AuthService
) {}

  canLoad(route: Route, segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {
    const currentUser = this.authService.isLoggedIn;
    const roles = this.authService.role;

    if ((currentUser && roles === 'Customer') ||
        (currentUser && roles === 'Barbershop') ||
        (currentUser && roles === 'Admin')) {
        return true;
    }

    this.router.navigateByUrl('/login');
    return false;
  }

}
