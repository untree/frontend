import { TestBed } from '@angular/core/testing';

import { HairstylistsService } from './hairstylists.service';

describe('HairstylistsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HairstylistsService = TestBed.get(HairstylistsService);
    expect(service).toBeTruthy();
  });
});
