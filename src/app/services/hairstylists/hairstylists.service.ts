import { Hairstylist } from './../../models/hairstylist-list.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EnvService } from '../env/env.service';

@Injectable({
  providedIn: 'root'
})
export class HairstylistsService {

  constructor(
    private http: HttpClient,
    private env: EnvService,
  ) { }

  // GET All Hairstylists filtered by BARBESHOP
  getAllHairstylist(id: string) {
    return this.http.get<Hairstylist[]>(this.env.API_URL + 'auth/hairstylist/' + id);
  }

  // GET One hairstlist
  getOneHairstyliyst(barberId: string, hairstylistId: string) {
    return this.http.get<Hairstylist>(this.env.API_URL + 'auth/hairstylist/' + barberId + '/' + hairstylistId);
  }

  // Add hairstylist
  addHairstylist(hairstylist_name: string, barbershop_id: string) {
    return this.http.post<Hairstylist>(this.env.API_URL + 'auth/hairstylist/add', {
      hairstylist_name: hairstylist_name,
      barbershop_id: barbershop_id
    });
  }

  // Edit hairstylist
  editHairstylist(hairstylist_id: string, hairstylist_name: string) {
    return this.http.post<Hairstylist>(this.env.API_URL + 'auth/hairstylist/edit/' + hairstylist_id, {
      hairstylist_id: hairstylist_id,
      hairstylist_name: hairstylist_name
    });
  }

  // Delete hairstylist
  deleteHairstylist(hairstylist_id: string, id: string) {
    return this.http.post(this.env.API_URL + 'auth/hairstylist/delete/' + hairstylist_id, {
      hairstylist_id: hairstylist_id,
      id: id
    });
  }
}
