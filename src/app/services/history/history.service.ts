import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EnvService } from '../env/env.service';
import { Queue } from 'src/app/models/queues.model';

@Injectable({
  providedIn: 'root'
})
export class HistoryService {

  constructor(
    private env: EnvService,
    private http: HttpClient
  ) { }

  // (POST) To store to history when the order is completed or canceled
  storeHistory(id: number, barbershop_id: number, hairstylist_id: number, user_id: number, customer_name: string, status: string) {
    return this.http.post(this.env.API_URL + 'auth/history/store', {
      id: id,
      barbershop_id: barbershop_id,
      hairstylist_id: hairstylist_id,
      user_id: user_id,
      customer_name: customer_name,
      status: status
    });
  }

  // GET History for CUSTOMER
  getHistoryCustomer(user_id: string) {
    return this.http.get<Queue[]>(this.env.API_URL + 'auth/history/' + user_id);
  }

  // GET History for BARBERSHOP
  getHistoryBarbershop(barber_id: string) {
    return this.http.get<Queue[]>(this.env.API_URL + 'auth/history/barbershop/' + barber_id);
  }

  // GET History Details
  getHistoryDetails(historyId: string) {
    return this.http.get<Queue>(this.env.API_URL + 'auth/history/details/' + historyId);
  }

  getHistoryCustomerDetails(historyId: string) {
    return this.http.get<Queue>(this.env.API_URL + 'auth/history/details/customer/' + historyId);
  }
}
