import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor(private toastController: ToastController) { }

  // PRESENT TOAST
  async presentToast(message: any) {
    const toast   = await this.toastController.create({
        message   : message,
        duration  : 2000,
        cssClass  : "toast",
        color     : 'dark'
    });

    toast.present();
}
}
