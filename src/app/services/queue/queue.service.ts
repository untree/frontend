import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EnvService } from '../env/env.service';
import { Queue } from 'src/app/models/queues.model';

@Injectable({
  providedIn: 'root'
})
export class QueueService {

  constructor(
    private http: HttpClient,
    private env: EnvService,
  ) { }

  // (POST) To Book Order (Queue)
  bookQueue(barbershop_id: number, hairstylist_id: number, user_id: number) {
    return this.http.post(this.env.API_URL + 'auth/queue/store', {
      barbershop_id: barbershop_id,
      hairstylist_id: hairstylist_id,
      user_id: user_id
    });
  }

  // Get Order Queue filtered for CUSTOMER
  getQueuesCustomer(user_id: string) {
    return this.http.get<Queue[]>(this.env.API_URL + 'auth/queue/user/' + user_id);
  }

  // Get Order Queue filtered for BARBERSHOP
  getQueuesBarbershop(barbershop_id: string) {
    return this.http.get<Queue[]>(this.env.API_URL + 'auth/queue/barber/' + barbershop_id);
  }

  // Get Order Queue Details
  getQueueDetails(queue_id: string) {
    return this.http.get<Queue>(this.env.API_URL + 'auth/queue/' + queue_id);
  }

  // Get Order Manual Customer Details
  getQueueCustomerDetails(queue_id: string) {
    return this.http.get<Queue>(this.env.API_URL + 'auth/queue/customer/' + queue_id);
  }

  // (POST) To add customer manually
  addCustomer(customer_name: string, barbershop_id: number, hairstylist_id: number) {
    return this.http.post(this.env.API_URL + 'auth/queue/customer/store', {
      customer_name: customer_name,
      barbershop_id: barbershop_id,
      hairstylist_id: hairstylist_id
    });
  }
}
