import { EnvService } from './../env/env.service';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { User } from '../../models/users.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isLoggedIn = false;
  token: any;
  role: string;
  scope: string;

  constructor(
    private http: HttpClient,
    private storage: NativeStorage,
    private env: EnvService
  ) {}

  // (POST) LOGIN SERVICE
  login(username: string, password: string) {
    return this.http.post<User>(this.env.API_URL + 'auth/login', {
        username: username,
        password: password
      })
      .pipe(tap(token => {
          this.storage.setItem('token', token).then(
            () => console.log('Token Stored'),
            error => console.log('Error storing item', error)
          );

          this.token = token;
          this.isLoggedIn = true;
          this.role = token.role;
          this.scope = (token.scope !== '' ? token.scope : '');
          return token;
        })
      );
  }

  // (POST) REGISTER SERVICE
  register(
    first_name: string,
    last_name: string,
    username: string,
    email: string,
    phone: string,
    password: string
  ) {
    return this.http.post(this.env.API_URL + 'auth/register', {
      first_name: first_name,
      last_name: last_name,
      username: username,
      email: email,
      phone: phone,
      password: password
    });
  }

  // LOGOUT SERVICE
  logout() {
    const headers = new HttpHeaders({
      Authorization: this.token['token_type'] + ' ' + this.token['access_token']
    });

    return this.http.get(this.env.API_URL + 'auth/logout', { headers: headers })
      .pipe(
        tap(data => {
          this.storage.remove('token');
          this.isLoggedIn = false;
          delete this.token;
          return data;
        })
      );
  }

  // GET USER with Token, Role, and Scope
  user() {
    const headers = new HttpHeaders({
      Authorization: this.token['token_type'] + ' ' + this.token['access_token']
    });

    return this.http.get<User>(this.env.API_URL + 'auth/user', { headers: headers })
      .pipe(
        tap(user => {
          this.role = user.role;
          this.scope = user.scope;
          return user;
        })
      );
  }

  // GET TOKEN
  getToken() {
    return this.storage.getItem('token').then(
      data => {
        this.token = data;
        this.isLoggedIn = this.token !== null ? true : false;
      },
      error => {
        this.token = null;
        this.isLoggedIn = false;
      }
    );
  }
}
