import { EnvService } from './../env/env.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from 'src/app/models/users.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private http: HttpClient,
    private env: EnvService
  ) { }

  // Update account
  editAccount(userId: string, first_name: string, last_name: string, email: string, phone: string, username: string) {
    return this.http.post<User>(this.env.API_URL + 'auth/edit/user/' + userId, {
      first_name: first_name,
      last_name: last_name,
      email: email,
      phone: phone,
      username: username
    });
  }

  // Change Password
  changePassword(userId: string, old_password: string, new_password: string, conf_password: string) {
    return this.http.post<User>(this.env.API_URL + 'auth/change/password/' + userId, {
      old_password: old_password,
      password: new_password,
      conf_password: conf_password
    });
  }

  // Forgot Password
  forgotPassword(email: string) {
    return this.http.post(this.env.API_URL + 'auth/forgot/password/' + email, {
      email: email
    });
  }

  // Get all users for set barbershop user
  getAllUsers(barbershop_id: string) {
    return this.http.get<User[]>(this.env.API_URL + 'auth/get/users/' + barbershop_id);
  }
}
