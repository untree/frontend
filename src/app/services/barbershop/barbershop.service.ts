import { tap } from 'rxjs/operators';
import { Barbershop } from './../../models/barbershop.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EnvService } from '../env/env.service';

@Injectable({
  providedIn: 'root'
})
export class BarbershopService {
  barbershop: Barbershop;

  constructor(
    private http: HttpClient,
    private env: EnvService
    ) { }

  // Get all barbershops
  getAllBarbershops() {
    return this.http.get<Barbershop[]>(this.env.API_URL + 'auth/barbershop');
  }

  // Get all barbershops with hairstylists
  getBarbershopFilter() {
    return this.http.get<Barbershop[]>(this.env.API_URL + 'auth/barbershop/filter');
  }

  // Get one barbershop details
  getBarbershop(id: string) {
    return this.http.get<Barbershop>(this.env.API_URL + 'auth/barbershop/' + id)
    .pipe(tap(barbershop => {
          return barbershop;
      })
    );
  }

  // Create barbershop service
  addBarbershop(barberName: string, barberAddress: string, barberPhone: string, barberPrice: number, barberImage: string) {
    return this.http.post(this.env.API_URL + 'auth/admin/barbershop/add', {
      barbershop_name: barberName,
      address: barberAddress,
      phoneNumber: barberPhone,
      price: barberPrice,
      barberImage: barberImage
    });
  }

  // Update barbershop service
  updateBarbershop(
    barberId: string, barberName: string, barberAddress: string, barberPhone: string, barberPrice: number, barberImage: string) {
    return this.http.post(this.env.API_URL + 'auth/admin/barbershop/update/' + barberId, {
      barbershop_name: barberName,
      address: barberAddress,
      phoneNumber: barberPhone,
      price: barberPrice,
      barberImage: barberImage
    });
  }

  // Change role for barbershop users
  changeRole(user_id: string, barbershop_id: string) {
    return this.http.post(this.env.API_URL + 'auth/admin/change/role/' + user_id  + '/' + barbershop_id, {
      user_id: user_id,
      barberhsop_id: barbershop_id
    });
  }

  // Delete Barbershop Service
  deleteBarbershop(barberId: string) {
    return this.http.post(this.env.API_URL + 'auth/admin/barbershop/delete/' + barberId, {
      barberId: barberId
    });
  }
}
