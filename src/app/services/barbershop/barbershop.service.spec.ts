import { TestBed } from '@angular/core/testing';

import { BarbershopService } from './barbershop.service';

describe('BarbershopService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BarbershopService = TestBed.get(BarbershopService);
    expect(service).toBeTruthy();
  });
});
