import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { ImageSelectorComponent } from './image-selector/image-selector.component';

@NgModule({
    imports: [CommonModule, IonicModule],
    declarations: [ImageSelectorComponent],
    exports: [ImageSelectorComponent]
})

export class ComponentsModule {}
