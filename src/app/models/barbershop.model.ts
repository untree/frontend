export class Barbershop {
    barbershop_id: number;
    barbershop_name: string;
    address: string;
    phoneNumber: string;
    price: number;
    image_barbershop: string;
}
