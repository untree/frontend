export class Hairstylist {
    id: number;
    barbershop_id: number;
    hairstylist_id: number;
    hairstylist_name: string;
    user_id: number;
    queue_id: number;
    queue: number;
}
