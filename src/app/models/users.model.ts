export class User {
    id: number;
    first_name: string;
    last_name: string;
    full_name: string;
    username: string;
    email: string;
    phone: string;
    password: string;
    role: string;
    scope: string;
}
