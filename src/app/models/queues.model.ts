export class Queue {
    id: number;
    user_id: number;
    full_name: string;
    customer_name: string;
    phone: string;
    queue: number;
    hairstylist_id: number;
    hairstylist_name: string;
    barbershop_id: number;
    barbershop_name: string;
    address: string;
    phoneNumber: string;
    price: number;
    image_barbershop: string;
    status: string;
    created_at: string;
}
