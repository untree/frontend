import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { TabsService } from './services/tabs/tabs.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { IonicSelectableModule } from 'ionic-selectable';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';

// Firebase
var firebaseConfig = {
  apiKey: "AIzaSyApneS7lv7G9DnhzVkpv6Jk6ha3c7PT_Eg",
  authDomain: "untreeapp.firebaseapp.com",
  databaseURL: "https://untreeapp.firebaseio.com",
  projectId: "untreeapp",
  storageBucket: "untreeapp.appspot.com",
  messagingSenderId: "184830902305",
  appId: "1:184830902305:web:c551fd67e80f9246"
};

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    IonicSelectableModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    NativeStorage,
    TabsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
