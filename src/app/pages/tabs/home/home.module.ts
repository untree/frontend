import { ComponentsModule } from './../../../components/components.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { HomePage } from './home.page';
import { AddBarbershopPage } from './admin/add-barbershop/add-barbershop.page';
import { SetUserPage } from './admin/set-user/set-user.page';
import { EditBarbershopPage } from './admin/edit-barbershop/edit-barbershop.page';
import { IonicSelectableModule } from 'ionic-selectable';

const routes: Routes = [
  {
    path: '',
    component: HomePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ComponentsModule,
    IonicModule,
    IonicSelectableModule,
    RouterModule.forChild(routes)
  ],
  declarations: [HomePage, AddBarbershopPage, SetUserPage],
  entryComponents: [AddBarbershopPage, SetUserPage]
})
export class HomePageModule {}
