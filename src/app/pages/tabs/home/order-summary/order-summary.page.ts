import { AlertService } from './../../../../services/alert/alert.service';
import { AuthService } from './../../../../services/auth/auth.service';
import { BarbershopService } from '../../../../services/barbershop/barbershop.service';
import { HairstylistsService } from '../../../../services/hairstylists/hairstylists.service';
import { Router } from '@angular/router';
import { AlertController, LoadingController, ModalController } from '@ionic/angular';
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { QueueService } from 'src/app/services/queue/queue.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-order-summary',
  templateUrl: './order-summary.page.html',
  styleUrls: ['./order-summary.page.scss'],
})
export class OrderSummaryPage implements OnInit, OnDestroy {
  @Input() hairstylist_id: number;
  @Input() barber_id: string;

  barberName: string;
  barberAddress: string;
  barberPrice: number;
  hairstylistName: string;
  queue: number;
  userId: number;
  barberSubs: Subscription;
  hairstylistSubs: Subscription;
  queueSubs: Subscription;
  authSubs: Subscription;

  constructor(
    private modalCtrl: ModalController,
    private router: Router,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private barbershopService: BarbershopService,
    private hairstyleService: HairstylistsService,
    private queueService: QueueService,
    private authService: AuthService,
    private alertService: AlertService
  ) {}

  ngOnInit() {
    // Get user data
    this.authSubs = this.authService.user().subscribe(user => {
      this.userId = user.id;
    }, error => {
      if (error.status === 500) {
        this.alertService.presentToast('Internal server error.');
      } else {
        this.alertService.presentToast('Failed to connect to the internet, please try again.');
      }
    });
    this.getBarbershop();
    this.getHairstylist();
  }

  getBarbershop() {
    // Get barbershop data
    this.loadingCtrl.create({
      spinner: 'crescent'
    }).then(loading => {
      loading.present();
      this.barberSubs = this.barbershopService.getBarbershop(this.barber_id).subscribe(barber => {
        this.barberName = barber.barbershop_name;
        this.barberAddress = barber.address;
        this.barberPrice = barber.price;
        loading.dismiss();
      }, error => {
        loading.dismiss();
        if (error.status === 500) {
          this.alertService.presentToast('Internal server error.');
        } else {
          this.alertService.presentToast('Failed to connect to the internet, please try again.');
        }
      });
    });
  }

  getHairstylist() {
    // Get hairstylist data
    this.loadingCtrl.create({
      spinner: 'crescent'
    }).then(loading => {
      loading.present();
      this.hairstylistSubs = this.hairstyleService.getOneHairstyliyst(this.barber_id, this.hairstylist_id + '')
      .subscribe(hairstylist => {
        this.hairstylistName = hairstylist.hairstylist_name;
        this.queue = hairstylist.queue;
        loading.dismiss();
      }, error => {
        loading.dismiss();
        if (error.status === 500) {
          this.alertService.presentToast('Internal server error.');
        } else {
          this.alertService.presentToast('Failed to connect to the internet, please try again.');
        }
      });
    });
  }

  // Booking barbershop
  async bookBarbershop() {
    const alert = await this.alertCtrl.create({
      header: 'Confirmation',
      message: 'Are you sure you want to book this?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Cancel Booking');
          }
        }, {
          text: 'Book',
          handler: () => {
            this.loadingCtrl.create({
              spinner: 'crescent'
            }).then(loading => {
              loading.present();
              // Post Queue to server
              this.queueSubs = this.queueService.bookQueue(
                +this.barber_id,
                this.hairstylist_id,
                this.userId
              ).subscribe(queueId => {
                loading.dismiss();
                this.modalCtrl.dismiss(null, 'book');
                this.router.navigateByUrl('/tabs/tabs/orders/' + this.hairstylist_id + '/details/' + queueId + '', { replaceUrl: true });
              }, error => {
                loading.dismiss();
                if (error.status === 500) {
                  this.alertService.presentToast('Internal server error.');
                } else {
                  this.alertService.presentToast('Failed to connect to the internet, please try again.');
                }
              });
            });
          }
        }
      ]
    });
    await alert.present();
  }

  // Cancel Order (Dismiss modal)
  onCancel() {
    this.modalCtrl.dismiss(null, 'cancel');
  }

  ngOnDestroy() {
    if (this.barberSubs) { this.barberSubs.unsubscribe(); }
    if (this.hairstylistSubs) { this.hairstylistSubs.unsubscribe(); }
    if (this.queueSubs) { this.queueSubs.unsubscribe(); }
  }
}
