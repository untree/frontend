import { ComponentsModule } from './../../../../../components/components.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { EditBarbershopPage } from './edit-barbershop.page';

const routes: Routes = [
  {
    path: '',
    component: EditBarbershopPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    IonicModule,
    ComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [EditBarbershopPage],
  entryComponents: [EditBarbershopPage]
})
export class EditBarbershopPageModule {}
