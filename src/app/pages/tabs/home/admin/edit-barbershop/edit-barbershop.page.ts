import { BarbershopService } from './../../../../../services/barbershop/barbershop.service';
import { Subscription } from 'rxjs';
import { AlertService } from './../../../../../services/alert/alert.service';
import { Barbershop } from '../../../../../models/barbershop.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AlertController, ModalController, LoadingController } from '@ionic/angular';
import { Component, OnInit, Input, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-edit-barbershop',
  templateUrl: './edit-barbershop.page.html',
  styleUrls: ['./edit-barbershop.page.scss'],
})
export class EditBarbershopPage implements OnInit, OnDestroy {
  @Input() barbershop: Barbershop;  
  form: FormGroup;
  barberSubs: Subscription;

  constructor(
    private alertCtrl: AlertController,
    private modalCtrl: ModalController,
    private loadingCtrl: LoadingController,
    private barbershopService: BarbershopService,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    // Edit Barbershop Form
    this.form = new FormGroup({
      barberName: new FormControl(this.barbershop.barbershop_name, {
        updateOn: 'blur',
        validators: [Validators.required, Validators.minLength(3), Validators.maxLength(50)]
      }),
      barberAddress: new FormControl(this.barbershop.address, {
        updateOn: 'blur',
        validators: [Validators.required, Validators.minLength(3)]
      }),
      barberPhone: new FormControl(this.barbershop.phoneNumber.substr(3), {
        updateOn: 'blur',
        validators: [Validators.required, Validators.minLength(3)]
      }),
      barberPrice: new FormControl(this.barbershop.price, {
        updateOn: 'blur',
        validators: [Validators.required, Validators.min(1000)]
      }),
      barberImg: new FormControl(this.barbershop.image_barbershop, {
        updateOn: 'blur',
        validators: [Validators.required]
      }),
    });
  }

  // Edit Barbershop
  async editBarbershop() {
    console.log(this.form);
    if (!this.form.valid) { return; }

    const alert = await this.alertCtrl.create({
      header: 'Confirmation',
      message: 'Are you sure you want to update this barbershop?',
      buttons: [{
        text: 'Cancel',
        role: 'cancel',
        handler: () => { console.log('cancel'); }
      }, {
        text: 'Yes',
        handler: () => {
          this.loadingCtrl.create({
            spinner: 'crescent',
            keyboardClose: true
          }).then(loading => {
            loading.present();
            this.barberSubs = this.barbershopService.updateBarbershop(
              this.barbershop.barbershop_id + '',
              this.form.value['barberName'],
              this.form.value['barberAddress'],
              this.form.value['barberPhone'],
              this.form.value['barberPrice'],
              this.form.value['barberImg']
            ).subscribe(() => {
              this.modalCtrl.dismiss({barber: {
                barberName: this.form.value['barberName'],
                barberAddress: this.form.value['barberAddress'],
                barberPhone: this.form.value['barberPhone'],
                barberPrice: this.form.value['barberPrice'],
                barberImg: this.form.value['barberImg']
              }}, 'updated');
              loading.dismiss();
            }, error => {
              loading.dismiss();
              if (error.status === 500) {
                this.alertService.presentToast('Internal server error.');
              } else {
                this.alertService.presentToast('Failed to connect to the internet, please try again.');
              }
            });
          });
        }
      }]
    });
    await alert.present();
  }

  // Image selector
  onImagePicked(imageData: string) {
    console.log('imageData: ', imageData);
    this.form.patchValue({ barberImg: imageData });
  }

  // Cancel edit (Dismiss modal)
  onCancel() {
    this.modalCtrl.dismiss(null, 'cancel');
  }

  ngOnDestroy() {
    if (this.barberSubs) { this.barberSubs.unsubscribe(); }
  }
}
