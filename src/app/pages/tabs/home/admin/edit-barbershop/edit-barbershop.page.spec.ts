import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditBarbershopPage } from './edit-barbershop.page';

describe('EditBarbershopPage', () => {
  let component: EditBarbershopPage;
  let fixture: ComponentFixture<EditBarbershopPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditBarbershopPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditBarbershopPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
