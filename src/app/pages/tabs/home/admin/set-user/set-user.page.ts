import { BarbershopService } from './../../../../../services/barbershop/barbershop.service';
import { Subscription } from 'rxjs';
import { AlertService } from './../../../../../services/alert/alert.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ModalController, LoadingController, AlertController } from '@ionic/angular';
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { User } from 'src/app/models/users.model';

@Component({
  selector: 'app-set-user',
  templateUrl: './set-user.page.html',
  styleUrls: ['./set-user.page.scss'],
})
export class SetUserPage implements OnInit, OnDestroy {
  @Input() users: User[];
  @Input() barberId: number;
  @Input() user: User;
  form: FormGroup;
  barberSubs: Subscription;

  constructor(
    private modalCtrl: ModalController,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private barbershopService: BarbershopService,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    // Set User Customer form
    // If barbershop user has been set
    if (this.user) {
      this.form = new FormGroup({
        selected_user: new FormControl(this.user, {
          updateOn: 'blur',
          validators: [Validators.required]
        })
      });
    } else {
      this.form = new FormGroup({
        selected_user: new FormControl(null, {
          updateOn: 'blur',
          validators: [Validators.required]
        })
      });
    }
  }

  // Set Role from customer to barbershop for selected user and barbershop
  async setUser() {
    console.log(this.form);
    if (!this.form.valid) { return; }
    const alert = await this.alertCtrl.create({
      header: 'Confirmation',
      message: 'Are you sure you want to set this user user role to Barbershop?',
      buttons: [{
        text: 'Cancel',
        role: 'cancel'
      }, {
        text: 'Change Role',
        handler: () => {
          this.loadingCtrl.create({
            spinner: 'crescent',
            keyboardClose: true
          }).then(loading => {
            loading.present();
            // Post change role
            this.barberSubs = this.barbershopService.changeRole(
              this.form.value.selected_user.id,
              this.barberId + ''
            ).subscribe(() => {
              this.modalCtrl.dismiss(null, 'role');
              loading.dismiss();
            }, error => {
              loading.dismiss();
              if (error.status === 500) {
                this.alertService.presentToast('Internal server error.');
              } else {
                this.alertService.presentToast('Failed to connect to the internet, please try again.');
              }
            })
          });
        }
      }]
    });
    await alert.present();
  }

  // Dismiss modal
  onCancel() {
    this.modalCtrl.dismiss();
  }

  ngOnDestroy() {
    if (this.barberSubs) { this.barberSubs.unsubscribe(); }
  }
}
