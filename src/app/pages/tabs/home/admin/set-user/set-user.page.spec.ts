import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetUserPage } from './set-user.page';

describe('SetUserPage', () => {
  let component: SetUserPage;
  let fixture: ComponentFixture<SetUserPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetUserPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetUserPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
