import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddBarbershopPage } from './add-barbershop.page';

describe('AddBarbershopPage', () => {
  let component: AddBarbershopPage;
  let fixture: ComponentFixture<AddBarbershopPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddBarbershopPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddBarbershopPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
