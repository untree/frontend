import { ComponentsModule } from './../../../../../components/components.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { AddBarbershopPage } from './add-barbershop.page';

const routes: Routes = [
  {
    path: '',
    component: AddBarbershopPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    IonicModule,
    ComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AddBarbershopPage],
  entryComponents: [AddBarbershopPage]

})
export class AddBarbershopPageModule {}
