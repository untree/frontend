import { BarbershopService } from './../../../../../services/barbershop/barbershop.service';
import { AlertService } from './../../../../../services/alert/alert.service';
import { AlertController, ModalController, LoadingController } from '@ionic/angular';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-add-barbershop',
  templateUrl: './add-barbershop.page.html',
  styleUrls: ['./add-barbershop.page.scss'],
})
export class AddBarbershopPage implements OnInit, OnDestroy {
  form: FormGroup;
  barberSubs: Subscription;

  constructor(
    private modalCtrl: ModalController,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private barberService: BarbershopService,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    // Add barbershop Form
    this.form = new FormGroup({
      barberName: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required, Validators.minLength(3), Validators.maxLength(50)]
      }),
      barberAddress: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required, Validators.minLength(3)]
      }),
      barberPhone: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required, Validators.minLength(12)]
      }),
      barberPrice: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required, Validators.min(1000)]
      }),
      barberImg: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required]
      })
    });
  }

  // Add Barbershop function
  async addBarbershop() {
    console.log(this.form);
    if (!this.form.valid) { return; }

    const alert = await this.alertCtrl.create({
      header: 'Confirmation',
      message: 'Are you sure you want to add this barbershop?',
      buttons: [{
        text: 'Cancel',
        role: 'cancel',
        handler: () => { console.log('cancel'); }
      }, {
        text: 'Yes',
        handler: () => {
          this.loadingCtrl.create({
            spinner: 'crescent',
            keyboardClose: true
          }).then(loading => {
            loading.present();
            this.barberSubs = this.barberService.addBarbershop(
              this.form.value['barberName'],
              this.form.value['barberAddress'],
              this.form.value['barberPhone'],
              this.form.value['barberPrice'],
              this.form.value['barberImg'],
            ).subscribe(() => {
              this.modalCtrl.dismiss(null, 'added');
              loading.dismiss();
            }, error => {
              loading.dismiss();
              if (error.status === 500) {
                this.alertService.presentToast('Internal server error.');
              } else {
                this.alertService.presentToast('Failed to connect to the internet, please try again.');
              }
            });
          });
        }
      }]
    });
    await alert.present();
  }

  // Image selector
  onImagePicked(imageData: string) {
    console.log(imageData);
    this.form.patchValue({ barberImg: imageData });
  }

  // Dismiss modal (Cancel add barbershop)
  onCancel() {
    this.modalCtrl.dismiss(null, 'cancel');
  }

  ngOnDestroy() {
    if (this.barberSubs) { this.barberSubs.unsubscribe(); }
  }
}
