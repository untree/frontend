import { User } from 'src/app/models/users.model';
import { UserService } from './../../../services/user/user.service';
import { AlertService } from './../../../services/alert/alert.service';
import { AddBarbershopPage } from './admin/add-barbershop/add-barbershop.page';
import { SetUserPage } from './admin/set-user/set-user.page';
import { AuthService } from './../../../services/auth/auth.service';
import { Barbershop } from './../../../models/barbershop.model';
import { BarbershopService } from './../../../services/barbershop/barbershop.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { IonItemSliding, AlertController, LoadingController, ModalController } from '@ionic/angular';
import { EditBarbershopPage } from './admin/edit-barbershop/edit-barbershop.page';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit, OnDestroy {
  barbershops: Barbershop[];
  barbershop1: Barbershop[];
  barbershopsFilter: Barbershop[];
  errorMsg: string;
  userRole: string;
  authSubs: Subscription;
  users: User[];
  user: User;
  barberSubs: Subscription;
  barberSubs1: Subscription;

  constructor(
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private modalCtrl: ModalController,
    private barberService: BarbershopService,
    private authService: AuthService,
    private userService: UserService,
    private alertService: AlertService
    ) {
  }

  ngOnInit() {
    this.authSubs = this.authService.user().subscribe(user => {
      this.userRole = user.role;

      // Get barbershop based on role
      if (this.userRole === 'Customer') { this.getBarbershopsFilter(); }
      else { this.getBarbershops(); }
    }, error => {
      if (error.status === 500) {
        this.alertService.presentToast('Internal server error.');
      } else {
        this.alertService.presentToast('Failed to connect to the internet, please try again.');
      }
    });
  }

  // Get Barbershops Data from API
  getBarbershops() {
    this.barberSubs = this.barberService.getAllBarbershops().subscribe(barbers => {
      this.barbershops = barbers;
      this.barbershop1 = barbers;
    }, error => {
      this.errorMsg = error.message;
      if (error.status === 500) {
        this.alertService.presentToast('Internal server error.');
      } else {
        this.alertService.presentToast('Failed to connect to the internet, please try again.');
      }
    });
  }

  getBarbershopsFilter() {
    this.barberService.getBarbershopFilter().subscribe(barbers => {
      this.barbershopsFilter = barbers;
      this.barbershop1 = barbers;
      console.log(this.barbershopsFilter);
    }, error => {
      this.errorMsg = error.message;
      if (error.status === 500) {
        this.alertService.presentToast('Internal server error.');
      } else {
        this.alertService.presentToast('Failed to connect to the internet, please try again.');
      }
    });
  }

  // Search function
  searchBarbershop(ev: any) {
    // set val to the value of the searchbar
    const val = ev.target.value;

    if (this.userRole === 'Admin') {
      if (val.length === 0) { this.getBarbershops(); }
      // if the value is an empty string don't filter the items
      if (val && val.trim() !== '') {
        this.barbershops = this.barbershop1.filter((item) => {
          return (item.barbershop_name.toLowerCase().indexOf(val.toString().toLowerCase()) > -1);
        });
      }
    } else {
      if (val.length === 0) { this.getBarbershopsFilter(); }
      // if the value is an empty string don't filter the items
      if (val && val.trim() !== '') {
        this.barbershopsFilter = this.barbershop1.filter((item) => {
          return (item.barbershop_name.toLowerCase().indexOf(val.toString().toLowerCase()) > -1);
        });
      }
    }
  }

  // ========= ADMIN =============
  // Admin Add Barbershop
  addBarbershop() {
    this.modalCtrl.create({
      component: AddBarbershopPage
    }).then(modal => {
      modal.present();
      return modal.onDidDismiss();
    }).then(resultData => {
      if (resultData.role === 'added') {
        this.loadingCtrl.create({
          spinner: 'crescent'
        }).then(loading => {
          loading.present();
          this.barberSubs = this.barberService.getAllBarbershops().subscribe(data => {
            this.barbershops = data;
            loading.dismiss();
            this.alertService.presentToast('Successfully add barbershop.');
          }, error => {
            loading.dismiss();
            if (error.status === 500) {
              this.alertService.presentToast('Internal server error.');
            } else {
              this.alertService.presentToast('Failed to connect to the internet, please try again.');
            }
          });
        });
      }
    });
  }

  // Admin Edit Barbershop
  editBarbershop(barbershop: Barbershop, slidingItem: IonItemSliding) {
    slidingItem.close();
    this.modalCtrl.create({
      component: EditBarbershopPage,
      componentProps: { barbershop: barbershop }
    }).then(modal => {
      modal.present();
      return modal.onDidDismiss();
    }).then(resultData => {
      if (resultData.role === 'updated') {
        this.loadingCtrl.create({
          spinner: 'crescent'
        }).then(loading => {
          loading.present();
          this.barberSubs = this.barberService.getAllBarbershops().subscribe(data => {
            this.barbershops = data;
            loading.dismiss();
            this.alertService.presentToast('Successfully update barbershop.');
          }, error => {
            loading.dismiss();
            if (error.status === 500) {
              this.alertService.presentToast('Internal server error.');
            } else {
              this.alertService.presentToast('Failed to connect to the internet, please try again.');
            }
          });
        });
      }
    });
  }

  // Set User for Barbershop
  setUser(barberId: string, slidingItem: IonItemSliding) {    
    slidingItem.close();

    // Get User Data
    this.userService.getAllUsers(barberId).subscribe(users => {
      this.users = users;

      // If barbershop user has been set
      this.user = this.users.find(e => {
        return e.role === 'Barbershop';
      });
      // Open modal to set user page
      this.modalCtrl.create({
        component: SetUserPage,
        componentProps: {
          users: this.users,
          barberId: barberId,
          user: this.user
        }
      }).then(modal => {
        modal.present();
        return modal.onDidDismiss();
      }).then(resultData => {
        if (resultData.role === 'role') {
          this.alertService.presentToast('Successfully set user role to barbershop.');
        }
      });
    }, error => {
      if (error.status === 500) {
        this.alertService.presentToast('Internal server error.');
      } else {
        this.alertService.presentToast('Failed to connect to the internet, please try again.');
      }
    });
  }

  // Admin Delete Barbershop
  async deleteBarbershop(barberId: string, slidingItem: IonItemSliding) {
    slidingItem.close();

    const alert = await this.alertCtrl.create({
      header: 'Confirmation',
      message: 'Are you sure you want to delete this Barbershop?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Delete',
          handler: () => {
            this.barberSubs1 = this.barberService.deleteBarbershop(barberId).subscribe(() => {
              console.log('delete');
            }, error => {
              if (error.status === 500) {
                this.alertService.presentToast('Internal server error.');
              } else {
                this.alertService.presentToast('Failed to connect to the internet, please try again.');
              }
            });
            this.loadingCtrl.create({
              spinner: 'crescent'
            }).then(loading => {
              loading.present();
              setTimeout(() => {
                this.barberSubs = this.barberService.getAllBarbershops().subscribe(data => {
                  this.barbershops = data;
                  loading.dismiss();
                  this.alertService.presentToast('Successfuly delete barbershop.');
                }, error => {
                  loading.dismiss();
                  if (error.status === 500) {
                    this.alertService.presentToast('Internal server error.');
                  } else {
                    this.alertService.presentToast('Failed to connect to the internet, please try again.');
                  }
                });
              }, 1000);
            });
          }
        }
      ]
    });
    await alert.present();
  }

  ngOnDestroy() {
    if (this.authSubs) { this.authSubs.unsubscribe(); }
    if (this.barberSubs) { this.barberSubs.unsubscribe(); }
    if (this.barberSubs1) { this.barberSubs1.unsubscribe(); }
  }
}
