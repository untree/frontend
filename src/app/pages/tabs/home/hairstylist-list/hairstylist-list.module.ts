import { OrderSummaryPage } from './../order-summary/order-summary.page';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { HairstylistListPage } from './hairstylist-list.page';

const routes: Routes = [
  {
    path: '',
    component: HairstylistListPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [HairstylistListPage, OrderSummaryPage],
  entryComponents: [OrderSummaryPage]
})
export class HairstylistListPageModule {}
