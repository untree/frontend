import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HairstylistListPage } from './hairstylist-list.page';

describe('HairstylistListPage', () => {
  let component: HairstylistListPage;
  let fixture: ComponentFixture<HairstylistListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HairstylistListPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HairstylistListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
