import { AlertService } from './../../../../services/alert/alert.service';
import { OrderSummaryPage } from './../order-summary/order-summary.page';
import { Hairstylist } from './../../../../models/hairstylist-list.model';
import { HairstylistsService } from './../../../../services/hairstylists/hairstylists.service';
import { BarbershopService } from './../../../../services/barbershop/barbershop.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController, ModalController } from '@ionic/angular';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-hairstylist-list',
  templateUrl: './hairstylist-list.page.html',
  styleUrls: ['./hairstylist-list.page.scss']
})
export class HairstylistListPage implements OnInit, OnDestroy {
  barberName: string;
  barberId: number;
  barberImg: string;
  hairstylists: Hairstylist[];
  hairstylistsSubs: Subscription;
  barberSubs: Subscription;

  constructor(
    private navCtrl: NavController,
    private route: ActivatedRoute,
    private modalCtrl: ModalController,
    private barbershopService: BarbershopService,
    private hairstyleService: HairstylistsService,
    private alertService: AlertService
  ) {}

  ngOnInit() {
    this.route.paramMap.subscribe(paramMap => {
      if (!paramMap.has('barbershopId')) {
        this.navCtrl.navigateBack('tabs/tabs/home');
        return;
      }
      this.getBarbershop(paramMap.get('barbershopId'));
      this.getHairstylists(paramMap.get('barbershopId'));
    });
  }

  // Get one barbershop data from api
  getBarbershop(paramMap: string) {
    this.barberSubs = this.barbershopService.getBarbershop(paramMap).subscribe(barbershop => {
      this.barberId = barbershop.barbershop_id;
      this.barberName = barbershop.barbershop_name;
      this.barberImg = barbershop.image_barbershop;
    }, error => {
      if (error.status === 500) {
        this.alertService.presentToast('Internal server error.');
      } else {
        this.alertService.presentToast('Failed to connect to the internet, please try again.');
      }
    });
  }

  // Get hairstylists filtered by Barbershop Id
  getHairstylists(paramMap: string) {
    this.hairstylistsSubs = this.hairstyleService.getAllHairstylist(paramMap).subscribe(data => {
      this.hairstylists = data;
    }, error => {
      if (error.status === 500) {
        this.alertService.presentToast('Internal server error.');
      } else {
        this.alertService.presentToast('Failed to connect to the internet, please try again.');
      }
    });
  }

  // Open modal to Order Summary after choosing one hairstylist
  orderSummary(hairstylist_id: string) {
    this.modalCtrl.create({
      component: OrderSummaryPage,
      componentProps: {
        hairstylist_id: hairstylist_id,
        barber_id: this.barberId
      }
    }).then(modal => {
      modal.present();
    });
  }

  ngOnDestroy() {
    if (this.barberSubs) { this.barberSubs.unsubscribe(); }
    if (this.hairstylistsSubs) { this.hairstylistsSubs.unsubscribe(); }
  }
}
