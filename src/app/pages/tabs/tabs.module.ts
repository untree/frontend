import { ReactiveFormsModule } from '@angular/forms';
import { TabsRoutingModule } from './tabs-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IonicModule } from '@ionic/angular';

import { TabsPage } from './tabs.page';
import { ComponentsModule } from 'src/app/components/components.module';
import { EditBarbershopPage } from './home/admin/edit-barbershop/edit-barbershop.page';

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    ReactiveFormsModule,
    ComponentsModule,
    TabsRoutingModule
  ],
  declarations: [TabsPage, EditBarbershopPage],
  entryComponents: [EditBarbershopPage]
})
export class TabsPageModule {}
