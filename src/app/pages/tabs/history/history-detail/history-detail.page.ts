import { LoadingController } from '@ionic/angular';
import { AlertService } from './../../../../services/alert/alert.service';
import { AuthService } from './../../../../services/auth/auth.service';
import { ActivatedRoute } from '@angular/router';
import { HistoryService } from './../../../../services/history/history.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-history-detail',
  templateUrl: './history-detail.page.html',
  styleUrls: ['./history-detail.page.scss'],
})
export class HistoryDetailPage implements OnInit, OnDestroy {
  userRole: string;
  historyId: number;
  status: string;
  orderTime: string;
  barberName: string;
  barberAddress: string;
  price: number;
  hairstylistName: string;
  fullName: string;
  custName: string;
  authSubs: Subscription;
  historySubs: Subscription;

  constructor(
    private route: ActivatedRoute,
    private loadingCtrl: LoadingController,
    private authService: AuthService,
    private historyService: HistoryService,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    this.authSubs = this.authService.user().subscribe(user => {
      this.userRole = user.role;
    }, error => {
      if (error.status === 500) {
        this.alertService.presentToast('Internal server error.');
      } else {
        this.alertService.presentToast('Failed to connect to the internet, please try again.');
      }
    });

    this.route.paramMap.subscribe(paramMap => {
      if (paramMap.get('custId') !== 'GST') this.getHistoryDetails(paramMap.get('historyId'));
      else this.getHistoryCustomerDetails(paramMap.get('historyId'));
    });
  }

  getHistoryDetails(param: string) {
    this.loadingCtrl.create({
      spinner: 'crescent'
    }).then(loading => {
      loading.present();

      this.historySubs = this.historyService.getHistoryDetails(param).subscribe(history => {
        this.historyId = history.id;
        this.status = history.status;
        this.orderTime = history.created_at;
        this.barberName = history.barbershop_name;
        this.barberAddress = history.address;
        this.price = history.price;
        this.hairstylistName = history.hairstylist_name;
        this.fullName = history.full_name;
        loading.dismiss();
      }, error => {
        loading.dismiss();
        if (error.status === 500) {
          this.alertService.presentToast('Internal server error.');
        } else {
          this.alertService.presentToast('Failed to connect to the internet, please try again.');
        }
      });
    });
  }

  getHistoryCustomerDetails(param: string) {
    this.loadingCtrl.create({
      spinner: 'crescent'
    }).then(loading => {
      loading.present();

      this.historySubs = this.historyService.getHistoryCustomerDetails(param).subscribe(history => {
        this.historyId = history.id;
        this.status = history.status;
        this.orderTime = history.created_at;
        this.barberName = history.barbershop_name;
        this.barberAddress = history.address;
        this.price = history.price;
        this.hairstylistName = history.hairstylist_name;
        this.custName = history.customer_name;
        loading.dismiss();
      }, error => {
        loading.dismiss();
        if (error.status === 500) {
          this.alertService.presentToast('Internal server error.');
        } else {
          this.alertService.presentToast('Failed to connect to the internet, please try again.');
        }
      });
    });
  }

  ngOnDestroy() {
    if (this.authSubs) { this.authSubs.unsubscribe(); }
    if (this.historySubs) { this.historySubs.unsubscribe(); }
  }
}
