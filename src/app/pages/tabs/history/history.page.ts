import { Subscription } from 'rxjs';
import { AlertService } from './../../../services/alert/alert.service';
import { AuthService } from './../../../services/auth/auth.service';
import { HistoryService } from './../../../services/history/history.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Queue } from 'src/app/models/queues.model';

@Component({
  selector: 'app-history',
  templateUrl: './history.page.html',
  styleUrls: ['./history.page.scss'],
})
export class HistoryPage implements OnInit, OnDestroy {
  userId: number;
  barberId: number;
  userRole: string;
  userHistory: Queue[];
  barberHistory: Queue[];
  errorMsg: string;
  historySubs: Subscription;
  authSubs: Subscription;

  constructor(
    private historyService: HistoryService,
    private authService: AuthService,
    private alertService: AlertService
    ) { }

  ngOnInit() {}

  ionViewWillEnter() {
    // Get User details
    this.authSubs = this.authService.user().subscribe(user => {
      this.userId = user.id;
      this.barberId = +user.scope;
      this.userRole = user.role;

      // Call function based on role
      if (user.role === 'Customer') {
        this.getHistoryCustomer(this.userId + '');
      } else if (user.role === 'Barbershop') {
        this.getHistoryBarbershop(this.barberId + '');
      }
    }, error => {
      if (error.status === 500) {
        this.alertService.presentToast('Internal server error.');
      } else {
        this.alertService.presentToast('Failed to connect to the internet, please try again.');
      }
    });
  }

  // For Customer
  getHistoryCustomer(userId: string) {
    this.historySubs = this.historyService.getHistoryCustomer(userId).subscribe(history => {
      this.userHistory = history;
      console.log(this.userHistory);
      if (this.userHistory === null) {
        this.errorMsg = 'You have no history. Order Now!';
      } else {
        this.errorMsg = '';
      }
    }, error => {
      if (error.status === 500) {
        this.alertService.presentToast('Internal server error.');
      } else {
        this.alertService.presentToast('Failed to connect to the internet, please try again.');
      }
    });
  }

  // For Partner
  getHistoryBarbershop(barberId: string) {
    this.historySubs = this.historyService.getHistoryBarbershop(barberId).subscribe(history => {
      this.barberHistory = history;
      console.log(this.barberHistory);
      if (this.barberHistory === null) {
        this.errorMsg = 'You have no history.';
      } else {
        this.errorMsg = '';
      }
    }, error => {
      if (error.status === 500) {
        this.alertService.presentToast('Internal server error.');
      } else {
        this.alertService.presentToast('Failed to connect to the internet, please try again.');
      }
    });
  }

  // Refresher
  doRefresh(event: any) {
    console.log('Begin async operation ', event);
    this.ionViewWillEnter();

    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 1000);
  }

  ngOnDestroy() {
    if (this.authSubs) { this.authSubs.unsubscribe(); }
    if (this.historySubs) { this.historySubs.unsubscribe(); }
  }
}
