import { TabsPage } from './tabs.page';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'home',
        children: [
          {
            path: '',
            children: [
              {
                path: '',
                loadChildren: './home/home.module#HomePageModule'
              },
              {
                path: ':barbershopId',
                children: [
                  {
                    path: '',
                    loadChildren: './home/hairstylist-list/hairstylist-list.module#HairstylistListPageModule'
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        path: 'orders',
        children: [
          {
            path: '',
            children: [
              {
                path: '',
                loadChildren: './orders/orders.module#OrdersPageModule'
              },
              {
                path: ':hairstylist_id/details/:orderId',
                loadChildren: './orders/order-detail/order-detail.module#OrderDetailPageModule'
              },
              {
                path: ':hairstylist_id/details/:orderId/:custId/manual',
                loadChildren: './orders/order-detail/order-detail.module#OrderDetailPageModule'
              },
              {
                path: 'chats',
                loadChildren: './orders/chats/chats.module#ChatsPageModule'
              },
            ]
          }
        ]
      },
      {
        path: 'history',
        children: [
          {
            path: '',
            children: [
              {
                path: '',
                loadChildren: './history/history.module#HistoryPageModule'
              },
              {
                path: 'details/:historyId',
                loadChildren: './history/history-detail/history-detail.module#HistoryDetailPageModule'
              },
              {
                path: 'details/:historyId/:custId/manual',
                loadChildren: './history/history-detail/history-detail.module#HistoryDetailPageModule'
              }
            ]
          }
        ]
      },
      {
        path: 'account',
        children: [
          {
            path: '',
            loadChildren: './account/account.module#AccountPageModule'
          }
        ]
      },
      {
        path: 'manage-hairstylists',
        children: [
          {
            path: '',
            loadChildren: './manage-hairstylist/manage-hairstylist.module#ManageHairstylistPageModule'
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/tabs/home',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/tabs/home',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsRoutingModule {}
