import { AngularFireDatabase } from '@angular/fire/database';
import { ChatsPage } from './../chats/chats.page';
import { Subscription } from 'rxjs';
import { AlertService } from './../../../../services/alert/alert.service';
import { AlertController, LoadingController, ModalController } from '@ionic/angular';
import { HistoryService } from './../../../../services/history/history.service';
import { AuthService } from './../../../../services/auth/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { QueueService } from 'src/app/services/queue/queue.service';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-order-detail',
  templateUrl: './order-detail.page.html',
  styleUrls: ['./order-detail.page.scss'],
})
export class OrderDetailPage implements OnInit, OnDestroy {
  userId: number;
  userRole: string;
  historyStatus: string;
  custId: number;
  fullName: string;
  custName: string;
  custPhone: string;
  queue: number;
  orderId: number;
  partnerId: number;
  barberId: number;
  barberName: string;
  barberAddress: string;
  barberPhone: string;
  price: number;
  orderTime: string;
  hairstylistName: string;
  authSubs: Subscription;
  queueSubs: Subscription;
  queueSubsCount: Subscription;
  historySubs: Subscription;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private modalCtrl: ModalController,
    private queueService: QueueService,
    private historyService: HistoryService,
    private authService: AuthService,
    private alertService: AlertService,
    public db: AngularFireDatabase
  ) { }

  ngOnInit() {
    // Get user details
    this.loadingCtrl.create({
      spinner: 'crescent'
    }).then(loading => {
      loading.present();
      this.authSubs = this.authService.user().subscribe(user => {
        this.userId = user.id;
        this.userRole = user.role;
        this.partnerId = +user.scope;
        loading.dismiss();

        this.route.paramMap.subscribe(paramMap => {
          if (paramMap.get('custId') !== 'GST') this.getQueueDetail(paramMap.get('orderId'));
          else this.getQueueCustomerDetail(paramMap.get('orderId'));
        });
      }, error => {
        loading.dismiss();
        if (error.status === 500) {
          this.alertService.presentToast('Internal server error.');
        } else {
          this.alertService.presentToast('Failed to connect to the internet, please try again.');
        }
      });
    });
  }

  // Get Order Queue Details
  getQueueDetail(paramMap: string) {
    this.loadingCtrl.create({
      spinner: 'crescent'
    }).then(loading => {
      loading.present();
      this.queueSubs = this.queueService.getQueueDetails(paramMap).subscribe(queue => {
        this.orderId = queue.id;
        this.barberId = queue.barbershop_id;
        this.queue = queue.queue;
        this.barberName = queue.barbershop_name;
        this.barberAddress = queue.address;
        this.price = queue.price;
        this.orderTime = queue.created_at;
        this.hairstylistName = queue.hairstylist_name;
        this.custId = queue.user_id;
        this.fullName = queue.full_name;
        loading.dismiss();
      }, error => {
        loading.dismiss();
        if (error.status === 500) {
          this.alertService.presentToast('Internal server error.');
        } else {
          this.alertService.presentToast('Failed to connect to the internet, please try again.');
        }
      });
    });
  }

  // Get Order Queue Details
  getQueueCustomerDetail(paramMap: string) {
    this.loadingCtrl.create({
      spinner: 'crescent'
    }).then(loading => {
      loading.present();
      this.queueSubs = this.queueService.getQueueCustomerDetails(paramMap).subscribe(queue => {
        this.orderId = queue.id;
        this.barberId = queue.barbershop_id;
        this.barberName = queue.barbershop_name;
        this.custId = queue.user_id;
        this.queue = queue.queue;
        this.custName = queue.customer_name;
        this.price = queue.price;
        this.orderTime = queue.created_at;
        this.hairstylistName = queue.hairstylist_name;       
        loading.dismiss();
      }, error => {
        loading.dismiss();
        if (error.status === 500) {
          this.alertService.presentToast('Internal server error.');
        } else {
          this.alertService.presentToast('Failed to connect to the internet, please try again.');
        }
      });
    });
  }

  // Chat for CUSTOMER
  chat() {
    if (this.userRole === 'Customer') {
      this.modalCtrl.create({
        component: ChatsPage,
        componentProps: { 
          orderId: this.orderId,
          username: this.fullName,
          userRole: this.userRole,
          barberName: this.barberName
        }
      }).then(modal => {
        modal.present();
      });
    } else {
      this.modalCtrl.create({
        component: ChatsPage,
        componentProps: { 
          orderId: this.orderId,
          username: this.barberName,
          userRole: this.userRole,
          fullName: this.fullName
        }
      }).then(modal => {
        modal.present();
      });
    }
  }

  // Call for Customer to Barbershop
  call() {
    if (this.userRole === 'Customer') window.open(`tel:${this.custPhone}`, '_system');
    else window.open(`tel:${this.barberPhone}`, '_system');
  }

  // Finish Order for BARBERSHOP
  async finishOrder() {
    const alert = await this.alertCtrl.create({
      header: 'Confirmation',
      message: 'Are you sure you want to finish this order?',
      buttons: [{
        text: 'No',
        role: 'cancel'
      }, {
        text: 'Yes',
        handler: () => {
          // Post to history table (status: Finish)
          this.loadingCtrl.create({
            spinner: 'crescent'
          }).then(loading => {
            loading.present();
            this.historyStatus = 'Completed';
            this.route.paramMap.subscribe(paramMap => {
              this.historySubs = this.historyService.storeHistory(
                this.orderId,
                this.partnerId,
                +paramMap.get('hairstylist_id'),
                this.custId,
                this.custName,
                this.historyStatus
              ).subscribe(() => {
                loading.dismiss();

                // Delete chat data from firebase
                this.db.object('/chat/' + this.orderId).remove();
                this.router.navigateByUrl('tabs/tabs/orders');
                this.alertService.presentToast('Order completed!');
              }, error => {
                loading.dismiss();
                if (error.status === 500) {
                  this.alertService.presentToast('Internal server error.');
                } else {
                  this.alertService.presentToast('Failed to connect to the internet, please try again.');
                }
              });
            });
          });
        }
      }]
    });
    await alert.present();
  }

  // Cancel Order for CUSTOMER
  async cancelOrder() {
    const alert = await this.alertCtrl.create({
      header: 'Confirmation',
      message: 'Are you sure you want to cancel this order?',
      buttons: [{
        text: 'No',
        role: 'cancel'
      }, {
        text: 'Yes',
        handler: () => {
          this.loadingCtrl.create({
            spinner: 'crescent'
          }).then(loading => {
            loading.present();
            this.historyStatus = 'Canceled';
            // Post to history table (status: Canceled)
            this.route.paramMap.subscribe(paramMap => {
              this.historySubs = this.historyService.storeHistory(
                this.orderId,
                this.barberId,
                +paramMap.get('hairstylist_id'),
                this.custId,
                this.custName,
                this.historyStatus
              ).subscribe(() => {
                loading.dismiss();

                // Delete data from firebase chat log
                this.db.object('/chat/' + this.orderId).remove();
                this.router.navigateByUrl('tabs/tabs/orders');
                this.alertService.presentToast('Order canceled.');
              }, error => {
                loading.dismiss();
                if (error.status === 500) {
                  this.alertService.presentToast('Internal server error.');
                } else {
                  this.alertService.presentToast('Failed to connect to the internet, please try again.');
                }
              });
            });
          });
        }
      }]
    });
    await alert.present();
  }

  // Refresher
  doRefresh(event: any) {
    console.log('Begin async operation ', event);
    this.route.paramMap.subscribe(paramMap => {
      if (paramMap.get('custId') !== 'GST') this.getQueueDetail(paramMap.get('orderId'));
      else this.getQueueCustomerDetail(paramMap.get('orderId'));
    });

    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 1000);
  }

  ngOnDestroy() {
    if (this.authSubs) { this.authSubs.unsubscribe(); }
    if (this.queueSubs) { this.queueSubs.unsubscribe(); }
    if (this.queueSubsCount) { this.queueSubsCount.unsubscribe(); }
    if (this.historySubs) { this.historySubs.unsubscribe(); }
  }
}
