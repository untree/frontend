import { Subscription } from 'rxjs';
import { AlertService } from './../../../../services/alert/alert.service';
import { AuthService } from './../../../../services/auth/auth.service';
import { Hairstylist } from './../../../../models/hairstylist-list.model';
import { HairstylistsService } from './../../../../services/hairstylists/hairstylists.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ModalController, AlertController } from '@ionic/angular';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { QueueService } from 'src/app/services/queue/queue.service';

@Component({
  selector: 'app-add-customer',
  templateUrl: './add-customer.page.html',
  styleUrls: ['./add-customer.page.scss'],
})
export class AddCustomerPage implements OnInit, OnDestroy {
  form: FormGroup;
  hairstylists: Hairstylist[];
  selectedHairstylist: number;
  barbershopId: number;
  customer_name: string;
  hairstylistsSubs: Subscription;
  authSubs: Subscription;
  queueSubs: Subscription;

  constructor(
    private modalCtrl: ModalController,
    private alertCtrl: AlertController,
    private hairstylistsService: HairstylistsService,
    private authService: AuthService,
    private queueService: QueueService,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    // Get user details
    this.authSubs = this.authService.user().subscribe(user => {
      this.barbershopId = +user.scope;
      this.getHairstylists(user.scope);
    }, error => {
      if (error.status === 500) {
        this.alertService.presentToast('Internal server error.');
      } else {
        this.alertService.presentToast('Failed to connect to the internet, please try again.');
      }
    });

    // Add Manual Customer form
    this.form = new FormGroup({
      customer_name: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required, Validators.minLength(3), Validators.maxLength(50)]
      }),
      selectedHairstylist: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required]
      })
    });
  }

  // Get hairstylists data for select hairstylist
  getHairstylists(barberId: string) {
    this.hairstylistsSubs = this.hairstylistsService.getAllHairstylist(barberId).subscribe(hairstylists => {
      this.hairstylists = hairstylists;
    }, error => {
      if (error.status === 500) {
        this.alertService.presentToast('Internal server error.');
      } else {
        this.alertService.presentToast('Failed to connect to the internet, please try again.');
      }
    });
  }

  // Add Customer (SUBMIT BUTTON)
  async addCustomer() {
    console.log(this.form);
    if (!this.form.valid) { return; }

    const alert = await this.alertCtrl.create({
      header: 'Confirmation',
      message: 'Are you sure you want to add this customer?',
      buttons: [{
        text: 'Cancel',
        role: 'cancel',
        handler: () => { console.log('cancel'); }
      }, {
        text: 'Yes',
        handler: () => {
          this.customer_name = this.form.value.customer_name;
          this.selectedHairstylist = this.form.value.selectedHairstylist;
          console.log(this.customer_name, this.barbershopId, this.selectedHairstylist);
          // POST Service
          this.queueSubs = this.queueService.addCustomer(
            this.customer_name,
            this.barbershopId,
            this.selectedHairstylist
          ).subscribe(data => {
            console.log(data);  
            // Dismiss modal
            this.modalCtrl.dismiss(null, 'add');
          }, error => {
            console.log(error);
            if (error.status === 500) {
              this.alertService.presentToast('Internal Server Error');
            } else {
              this.alertService.presentToast('Failed to connect to the internet, please try again.');
            }
          });
        }
      }]
    });
    await alert.present();
  }

  // Cancel to add customer (Dismiss Modal)
  onCancel() {
    this.modalCtrl.dismiss(null, 'cancel');
  }

  ngOnDestroy() {
    if (this.hairstylistsSubs) { this.hairstylistsSubs.unsubscribe(); }
    if (this.authSubs) { this.authSubs.unsubscribe(); }
    if (this.queueSubs) { this.queueSubs.unsubscribe(); }
  }
}
