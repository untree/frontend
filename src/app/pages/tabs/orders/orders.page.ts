import { AlertService } from './../../../services/alert/alert.service';
import { Subscription } from 'rxjs';
import { AddCustomerPage } from './add-customer/add-customer.page';
import { ModalController, LoadingController } from '@ionic/angular';
import { AuthService } from './../../../services/auth/auth.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { QueueService } from 'src/app/services/queue/queue.service';
import { Queue } from 'src/app/models/queues.model';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.page.html',
  styleUrls: ['./orders.page.scss'],
})

export class OrdersPage implements OnInit, OnDestroy {
  userId: number;
  userRole: string;
  barbershopId: string;
  queueCustomers: Queue[];
  queueBarbershops: Queue[];
  errorMsg = '';
  queueSubs: Subscription;
  authSubs: Subscription;

  constructor(
    private modalCtrl: ModalController,
    private loadingCtrl: LoadingController,
    private queueService: QueueService,
    private authService: AuthService,
    private alertService: AlertService
  ) { }

  ngOnInit() { }

  ionViewWillEnter() {
    // Get user detail
    this.authSubs = this.authService.user().subscribe(user => {
      this.userId = user.id;
      this.userRole = user.role;
      this.barbershopId = user.scope;

      // Call function based on role
      if (this.userRole === 'Customer') {
        this.getCustomerQueue();
      } else {
        this.getBarberQueue();
      }
    }, error => {
      if (error.status === 500) {
        this.alertService.presentToast('Internal server error.');
      } else {
        this.alertService.presentToast('Failed to connect to the internet, please try again.');
      }
    });
  }

  // Get Order Queue For Customer
  getCustomerQueue() {
    this.queueSubs = this.queueService.getQueuesCustomer(this.userId + '').subscribe(queues => {
      this.queueCustomers = queues;
      console.log(queues);
      // If customer has no order
      if (this.queueCustomers === null) {
        this.errorMsg = 'You have no order. Order now!';
      } else  { this.errorMsg = ''; }
    }, error => {
      if (error.status === 500) {
        this.alertService.presentToast('Internal server error.');
      } else {
        this.alertService.presentToast('Failed to connect to the internet, please try again.');
      }
    });
  }

  // Get Order Queue For Barbershop
  getBarberQueue() {
    this.queueSubs = this.queueService.getQueuesBarbershop(this.barbershopId).subscribe(queues => {
      this.queueBarbershops = queues;
      console.log(this.queueBarbershops);

      // If barbershop has no order
      if (this.queueBarbershops === null) {
        this.errorMsg = 'You have no order right now.';
      } else {
        this.errorMsg = '';
      }
    }, error => {
      if (error.status === 500) {
        this.alertService.presentToast('Internal server error.');
      } else {
        this.alertService.presentToast('Failed to connect to the internet, please try again.');
      }
    });
  }

  // (FAB) BARBERSHOP add customer manually
  addCustomer() {
    this.modalCtrl.create({
      component: AddCustomerPage
    }).then(modal => {
      modal.present();
      return modal.onDidDismiss();
    }).then(data => {
      if (data.role === 'add') {
        this.loadingCtrl.create({
          spinner: 'crescent'
        }).then(loading => {
          loading.present();
          this.queueSubs = this.queueService.getQueuesBarbershop(this.barbershopId).subscribe(queues => {
            loading.dismiss();
            this.queueBarbershops = queues;
            this.errorMsg = '';
            console.log(this.queueBarbershops);
            this.alertService.presentToast('Successfully add customer.');
          }, error => {
            loading.dismiss();
            if (error.status === 500) {
              this.alertService.presentToast('Internal server error.');
            } else {
              this.alertService.presentToast('Failed to connect to the internet, please try again.');
            }
          });
        });
      }
    });
  }

  // Refresher
  doRefresh(event: any) {
    console.log('Begin async operation ', event);
    this.ionViewWillEnter();

    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 1000);
  }

  ngOnDestroy() {
    if (this.authSubs) { this.authSubs.unsubscribe(); }
    if (this.queueSubs) { this.queueSubs.unsubscribe(); }
  }
}
