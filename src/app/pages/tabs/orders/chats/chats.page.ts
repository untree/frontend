import { AlertService } from './../../../../services/alert/alert.service';
import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { IonContent, ModalController } from '@ionic/angular';
import { AngularFireDatabase } from '@angular/fire/database';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-chats',
  templateUrl: './chats.page.html',
  styleUrls: ['./chats.page.scss'],
})
export class ChatsPage implements OnInit {
  @ViewChild(IonContent) content: IonContent;
  @Input() username: string;
  @Input() orderId: string;
  @Input() userRole: string;
  @Input() barberName: string;
  @Input() fullName: string;
  newMessage: string = '';
  messages: object[] = [];
  chatSub: Subscription;

  constructor(
    public db: AngularFireDatabase,
    private modalCtrl: ModalController,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    this.getMessages();
  }

  getMessages() {
    // Get the messages data
    this.chatSub = this.db.list('/chat/' + this.orderId).valueChanges().subscribe((data: any) => {
      this.messages = data;
      // Scroll to bottom after sending a chat
      setTimeout(() => {
        console.log('1: ', this.content);
        this.content.scrollToBottom(200);
      });
    }, error => {
      console.log('list: ', error);
      console.log('test list error');
      this.alertService.presentToast('Failed to connect to the internet, please try again.');      
    });
  }

  sendMessage() {
    // Send message data to firebase
    this.db.list('/chat/' + this.orderId).push({
      username: this.username,
      message: this.newMessage,
      createdAt: new Date().getTime()
    }).catch(error => {
      console.log('send: ', error);
      console.log('test send error');
      this.alertService.presentToast('Failed to connect to the internet, please try again.');
    });

    this.newMessage = '';
    // Scroll to bottom after sending a chat
    setTimeout(() => {
      console.log('2: ', this.content);      
      this.content.scrollToBottom(200);
    });
  }

  // Close modal
  onCancel() {
    this.modalCtrl.dismiss(null, 'cancel');
  }
}
