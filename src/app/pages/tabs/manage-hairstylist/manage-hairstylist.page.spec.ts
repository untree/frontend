import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageHairstylistPage } from './manage-hairstylist.page';

describe('ManageHairstylistPage', () => {
  let component: ManageHairstylistPage;
  let fixture: ComponentFixture<ManageHairstylistPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageHairstylistPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageHairstylistPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
