import { AlertService } from './../../../services/alert/alert.service';
import { AddHairstylistPage } from './add-hairstylist/add-hairstylist.page';
import { BarbershopService } from './../../../services/barbershop/barbershop.service';
import { Hairstylist } from './../../../models/hairstylist-list.model';
import { AuthService } from './../../../services/auth/auth.service';
import { HairstylistsService } from './../../../services/hairstylists/hairstylists.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ModalController, LoadingController, AlertController, IonItemSliding } from '@ionic/angular';
import { EditHairstylistPage } from './edit-hairstylist/edit-hairstylist.page';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-manage-hairstylist',
  templateUrl: './manage-hairstylist.page.html',
  styleUrls: ['./manage-hairstylist.page.scss'],
})
export class ManageHairstylistPage implements OnInit, OnDestroy {
  partnerId: string;
  barberImg: string;
  hairstylists: Hairstylist[];
  authSubs: Subscription;
  errorMsg: string;
  hairstylistSubs: Subscription;
  barberSubs: Subscription;

  constructor(
    private modalCtrl: ModalController,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private authService: AuthService,
    private hairstylistService: HairstylistsService,
    private barbershopService: BarbershopService,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    this.authSubs = this.authService.user().subscribe(user => {
      this.partnerId = user.scope;

      // Get Barbershop
      this.barberSubs = this.barbershopService.getBarbershop(this.partnerId).subscribe(barber => {
        this.barberImg = barber.image_barbershop;
      }, error => {
        if (error.status === 500) {
          this.alertService.presentToast('Internal server error.');
        } else {
          this.alertService.presentToast('Failed to connect to the internet, please try again.');
        }
      });

      // Get Hairstylists
      this.getHairstylists();
    });
  }
  // Get Hairstylists
  getHairstylists() {
    this.hairstylistSubs = this.hairstylistService.getAllHairstylist(this.partnerId).subscribe(hairstylists => {
      this.hairstylists = hairstylists;
      console.log(this.hairstylists);
      if (this.hairstylists === null) {
        this.errorMsg = 'You have no hairstylist. Add now!';
      } else  { this.errorMsg = ''; }
    }, error => {
      if (error.status === 500) {
        this.alertService.presentToast('Internal server error.');
      } else {
        this.alertService.presentToast('Failed to connect to the internet, please try again.');
      }
    });
  }

  // Add hairstylist
  addHairstylist() {
    this.modalCtrl.create({
      component: AddHairstylistPage
    }).then(modal => {
      modal.present();
      return modal.onDidDismiss();
    }).then(data => {
      if (data.role === 'add') {
        this.loadingCtrl.create({
          spinner: 'crescent'
        }).then(loading => {
          loading.present();
          // Load hairstylists
          this.hairstylistSubs = this.hairstylistService.getAllHairstylist(this.partnerId).subscribe(hairstylists => {
            this.hairstylists = hairstylists;
            console.log(this.hairstylists);
            if (this.hairstylists === null) {
              this.errorMsg = 'You have no hairstylist. Add now!';
            } else  { this.errorMsg = ''; }
            loading.dismiss();
            this.alertService.presentToast('Successfully add hairstylists');
          }, error => {
            loading.dismiss();
            if (error.status === 500) {
              this.alertService.presentToast('Internal server error.');
            } else {
              this.alertService.presentToast('Failed to connect to the internet, please try again.');
            }
          });
        });
      }
    });
  }

  // Edit hairstylist function
  editHairstylist(hairstylist: Hairstylist, slidingItem: IonItemSliding) {
    slidingItem.close();
    // Open edit barbershop modal
    this.modalCtrl.create({
      component: EditHairstylistPage,
      componentProps: { hairstylist: hairstylist }
    }).then(modal => {
      modal.present();
      return modal.onDidDismiss();
    }).then(resultData => {
      if (resultData.role === 'edit') {
        this.loadingCtrl.create({
          spinner: 'crescent'
        }).then(loading => {
          loading.present();
          // Load hairstylists
          this.hairstylistSubs = this.hairstylistService.getAllHairstylist(this.partnerId).subscribe(hairstylists => {
            this.hairstylists = hairstylists;
            loading.dismiss();
            this.alertService.presentToast('Successfully update hairstylist');
          }, error => {
            loading.dismiss();
            if (error.status === 500) {
              this.alertService.presentToast('Internal server error.');
            } else {
              this.alertService.presentToast('Failed to connect to the internet, please try again.');
            }
          });
        });
      }
    });
  }

  // Delete hairstylist
  async deleteHairstylist(hairstylist: Hairstylist, slidingItem: IonItemSliding) {
    slidingItem.close();

    const alert = await this.alertCtrl.create({
      header: 'Confirmation',
      message: 'Are you sure you want to delete this Barbershop?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Delete',
          handler: () => {
            // Post delete hairstylists
            this.hairstylistSubs = this.hairstylistService.deleteHairstylist(
              hairstylist.hairstylist_id + '',
              hairstylist.id + '').subscribe(() => {                
              }, error => {
                if (error.status === 500) {
                  this.alertService.presentToast('Internal server error.');
                } else {
                  this.alertService.presentToast('Failed to connect to the internet, please try again.');
                }
              });
            this.loadingCtrl.create({
              spinner: 'crescent'
            }).then(loading => {
              loading.present();
              setTimeout(() => {
                // Load hairstylists
                this.hairstylistSubs = this.hairstylistService.getAllHairstylist(this.partnerId).subscribe(hairstylists => {
                  this.hairstylists = hairstylists;
                  console.log(this.hairstylists);
                  if (this.hairstylists === null) {
                    this.errorMsg = 'You have no hairstylist. Add now!';
                  } else  { this.errorMsg = ''; }
                  loading.dismiss();
                  this.alertService.presentToast('Successfuly delete hairstylist.');
                }, error => {
                  loading.dismiss();
                  if (error.status === 500) {
                    this.alertService.presentToast('Internal server error.');
                  } else {
                    this.alertService.presentToast('Failed to connect to the internet, please try again.');
                  }
                });
              }, 1000);
            });
          }
        }
      ]
    });
    await alert.present();
  }

  doRefresh(event: any) {
    console.log('Begin async operation ', event);
    // Get Hairstylists
    this.getHairstylists();

    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 1000);
  }
  
  ngOnDestroy() {
    if (this.authSubs) { this.authSubs.unsubscribe(); }
    if (this.barberSubs) { this.barberSubs.unsubscribe(); }
    if (this.hairstylistSubs) { this.hairstylistSubs.unsubscribe(); }
  }
}
