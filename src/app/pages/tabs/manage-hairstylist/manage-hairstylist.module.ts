import { AddHairstylistPage } from './add-hairstylist/add-hairstylist.page';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ManageHairstylistPage } from './manage-hairstylist.page';
import { EditHairstylistPage } from './edit-hairstylist/edit-hairstylist.page';

const routes: Routes = [
  {
    path: '',
    component: ManageHairstylistPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ManageHairstylistPage, AddHairstylistPage, EditHairstylistPage],
  entryComponents: [AddHairstylistPage, EditHairstylistPage]
})
export class ManageHairstylistPageModule {}
