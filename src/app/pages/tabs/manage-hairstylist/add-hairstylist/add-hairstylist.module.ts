import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AddHairstylistPage } from './add-hairstylist.page';

const routes: Routes = [
  {
    path: '',
    component: AddHairstylistPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AddHairstylistPage],
  entryComponents: [AddHairstylistPage]
})
export class AddHairstylistPageModule {}
