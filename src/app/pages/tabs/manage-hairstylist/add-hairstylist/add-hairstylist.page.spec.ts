import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddHairstylistPage } from './add-hairstylist.page';

describe('AddHairstylistPage', () => {
  let component: AddHairstylistPage;
  let fixture: ComponentFixture<AddHairstylistPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddHairstylistPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddHairstylistPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
