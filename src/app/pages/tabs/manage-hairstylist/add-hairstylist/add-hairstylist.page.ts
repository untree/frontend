import { AuthService } from './../../../../services/auth/auth.service';
import { AlertService } from './../../../../services/alert/alert.service';
import { HairstylistsService } from './../../../../services/hairstylists/hairstylists.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ModalController, LoadingController, AlertController } from '@ionic/angular';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-add-hairstylist',
  templateUrl: './add-hairstylist.page.html',
  styleUrls: ['./add-hairstylist.page.scss'],
})
export class AddHairstylistPage implements OnInit, OnDestroy {
  form: FormGroup;
  barbershopId: string;
  hairstylistSubs: Subscription;
  authSubs: Subscription;

  constructor(
    private modalCtrl: ModalController,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private hairstylistService: HairstylistsService,
    private authService: AuthService,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    this.authSubs = this.authService.user().subscribe(user => {
      this.barbershopId = user.scope;
    })

    // Add hairstylist form
    this.form = new FormGroup({
      hairstylist_name: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required, Validators.minLength(3)]
      })
    });
  }

  // Add hairstylist function
  async addHairstylist() {
    console.log(this.form);
    if (!this.form.valid) { return; }
    console.log(this.barbershopId);
    
    const alert = await this.alertCtrl.create({
      header: 'Confirmation',
      message: 'Are you sure you want to add this hairstylist?',
      buttons: [{
        text: 'cancel',
        role: 'cancel'
      }, {
        text: 'Add Hairstylist',
        handler: () => {
          this.loadingCtrl.create({
            spinner: 'crescent',
            keyboardClose: true
          }).then(loading => {
            loading.present();
            // Post add hairstylist
            this.hairstylistSubs = this.hairstylistService.addHairstylist(
              this.form.value['hairstylist_name'],
              this.barbershopId
            ).subscribe(() => {
              this.modalCtrl.dismiss(null, 'add');
              loading.dismiss();
            }, error => {
              if (error) {
                loading.dismiss();
                if (error.status === 500) {
                  this.alertService.presentToast('Internal server error.');
                } else {
                  this.alertService.presentToast('Failed to connect to the internet, please try again.');
                }
              }
            });
          });
        }
      }]
    });
    alert.present();
  }

  onCancel() {
    this.modalCtrl.dismiss(null, 'cancel');
  }

  ngOnDestroy() {
    if (this.hairstylistSubs) { this.hairstylistSubs.unsubscribe(); }
    if (this.authSubs) { this.authSubs.unsubscribe(); }
  }
}
