import { AlertService } from './../../../../services/alert/alert.service';
import { Hairstylist } from './../../../../models/hairstylist-list.model';
import { HairstylistsService } from './../../../../services/hairstylists/hairstylists.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ModalController, LoadingController, AlertController } from '@ionic/angular';
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-edit-hairstylist',
  templateUrl: './edit-hairstylist.page.html',
  styleUrls: ['./edit-hairstylist.page.scss'],
})
export class EditHairstylistPage implements OnInit, OnDestroy {
  @Input() hairstylist: Hairstylist;
  form: FormGroup;
  hairstylistSubs: Subscription;

  constructor(
    private modalCtrl: ModalController,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private hairstylistService: HairstylistsService,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    // Edit hairstylist form
    this.form = new FormGroup({
      hairstylist_name: new FormControl(this.hairstylist.hairstylist_name, {
        updateOn: 'blur',
        validators: [Validators.required, Validators.minLength(3)]
      })
    });
  }

  // Edit hairstylist function
  async editHairstylist() {
    console.log(this.form);
    if (!this.form.valid) { return; }

    const alert = await this.alertCtrl.create({
      header: 'Confirmation',
      message: 'Are you sure you want to update this hairstylist?',
      buttons: [{
        text: 'Cancel',
        role: 'cancel'
      }, {
        text: 'Update',
        handler: () => {
          this.loadingCtrl.create({
            spinner: 'crescent',
            keyboardClose: true
          }).then(loading => {
            loading.present();
            // Post edit hairstylist
            this.hairstylistSubs = this.hairstylistService.editHairstylist(
              this.hairstylist.hairstylist_id + '',
              this.form.value['hairstylist_name']
            ).subscribe(() => {
              this.modalCtrl.dismiss(null, 'edit');
              loading.dismiss();
            }, error => {
              loading.dismiss();
              if (error.status === 500) {
                this.alertService.presentToast('Internal server error.');
              } else {
                this.alertService.presentToast('Failed to connect to the internet, please try again.');
              }
            });
          });
        }
      }]
    });
    await alert.present();
  }

  // Dismiss modal
  onCancel() {
    this.modalCtrl.dismiss(null, 'cancel');
  }

  ngOnDestroy() {
    if (this.hairstylistSubs) { this.hairstylistSubs.unsubscribe(); }
  }
}
