import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { EditHairstylistPage } from './edit-hairstylist.page';

const routes: Routes = [
  {
    path: '',
    component: EditHairstylistPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [EditHairstylistPage],
  entryComponents: [EditHairstylistPage]
})
export class EditHairstylistPageModule {}
