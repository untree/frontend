import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditHairstylistPage } from './edit-hairstylist.page';

describe('EditHairstylistPage', () => {
  let component: EditHairstylistPage;
  let fixture: ComponentFixture<EditHairstylistPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditHairstylistPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditHairstylistPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
