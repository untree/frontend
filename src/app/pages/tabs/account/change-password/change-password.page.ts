import { Subscription } from 'rxjs';
import { AlertService } from './../../../../services/alert/alert.service';
import { UserService } from './../../../../services/user/user.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ModalController, AlertController, NavController, LoadingController } from '@ionic/angular';
import { Component, OnInit, Input, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.page.html',
  styleUrls: ['./change-password.page.scss'],
})
export class ChangePasswordPage implements OnInit, OnDestroy {
  @Input() user_id: number;
  form: FormGroup;
  userSubs: Subscription;

  constructor(
    private alertCtrl: AlertController,
    private modalCtrl: ModalController,
    private navCtrl: NavController,
    private loadingCtrl: LoadingController,
    private userService: UserService,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    // Change password form
    this.form = new FormGroup({
      old_password: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required]
      }),
      new_password: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required, Validators.minLength(6), Validators.maxLength(16)]
      }),
      conf_password: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required, Validators.minLength(6), Validators.maxLength(16)]
      })
    });
  }

  async changePassword() {
    console.log(this.form);
    if (!this.form.valid) { return; }

    const alert = await this.alertCtrl.create({
      header: 'Confirmation',
      message: 'Are you sure you want to change your password?',
      buttons: [{
        text: 'Cancel',
        role: 'cancel',
        handler: () => { console.log('cancel'); }
      },
      {
        text: 'Yes',
        handler: () => {
          this.loadingCtrl.create({
            spinner: 'crescent',
            keyboardClose: true
          }).then(loading => {
            loading.present();
            // Post change password
            this.userSubs = this.userService.changePassword(
              this.user_id + '',
              this.form.value['old_password'],
              this.form.value['new_password'],
              this.form.value['conf_password'],
            ).subscribe(() => {
              loading.dismiss();
              this.modalCtrl.dismiss();
              this.navCtrl.navigateRoot('/login');
              this.alertService.presentToast('Successfully change password. Please login again.');
            }, error => {
              loading.dismiss();
              if (error.error.errors.old_password) {
                this.alertService.presentToast(error.error.errors.old_password);
              } else if (error.error.errors.new_password) {
                this.alertService.presentToast(error.error.errors.new_password);
              } else if (error.error.errors.password) {
                this.alertService.presentToast('Your password and confirmation password do not match.');
              } else if (error.status === 500) {
                this.alertService.presentToast('Internal server error.');
              } else {
                this.alertService.presentToast('Failed to connect to the internet, please try again.');
              }
            });
          });
        }
      }]
    });
    await alert.present();
  }

  onCancel() {
    this.modalCtrl.dismiss(null, 'cancel');
  }

  ngOnDestroy() {
    if (this.userSubs) { this.userSubs.unsubscribe(); }
  }
}
