import { ComponentsModule } from './../../../components/components.module';
import { EditBarbershopPage } from './../home/admin/edit-barbershop/edit-barbershop.page';
import { ChangePasswordPage } from './change-password/change-password.page';
import { EditAccountPage } from './edit-account/edit-account.page';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AccountPage } from './account.page';

const routes: Routes = [
  {
    path: '',
    component: AccountPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    IonicModule,
    ComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AccountPage, EditAccountPage, ChangePasswordPage],
  entryComponents: [EditAccountPage, ChangePasswordPage]
})
export class AccountPageModule {}
