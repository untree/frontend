import { Subscription } from 'rxjs';
import { AlertService } from './../../../services/alert/alert.service';
import { Barbershop } from './../../../models/barbershop.model';
import { EditBarbershopPage } from './../home/admin/edit-barbershop/edit-barbershop.page';
import { ChangePasswordPage } from './change-password/change-password.page';
import { BarbershopService } from './../../../services/barbershop/barbershop.service';
import { User } from 'src/app/models/users.model';
import { AuthService } from './../../../services/auth/auth.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { NavController, LoadingController, AlertController, ModalController } from '@ionic/angular';
import { EditAccountPage } from './edit-account/edit-account.page';

@Component({
  selector: 'app-account',
  templateUrl: './account.page.html',
  styleUrls: ['./account.page.scss'],
})
export class AccountPage implements OnInit, OnDestroy {
  user: User;
  user_id: number;
  first_name: string;
  last_name: string;
  username: string;
  email: string;
  phone: string;
  userRole: string;
  partnerId: string;
  barbershop: Barbershop;
  barberName: string;
  barberPhone: string;
  authSubs: Subscription;
  barberSubs: Subscription;

  constructor(
    private modalCtrl: ModalController,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private navCtrl: NavController,
    private authService: AuthService,
    private barberService: BarbershopService,
    private alertService: AlertService
    ) { }

  ngOnInit() {
    // Get user details
    this.authSubs = this.authService.user().subscribe(user => {
      this.user = user;
      this.user_id = user.id;
      this.first_name = user.first_name;
      this.last_name = user.last_name;
      this.email = user.email;
      this.username = user.username;
      this.phone = user.phone;
      this.userRole = user.role;
      this.partnerId = user.scope;

      // Get user details if login as BARBERSHOP
      if (this.userRole === 'Barbershop') {
        this.barberSubs = this.barberService.getBarbershop(this.partnerId).subscribe(barber => {
          this.barbershop = barber;
          this.barberName = barber.barbershop_name;
          this.barberPhone = barber.phoneNumber;
        }, error => {
          if (error.status === 500) {
            this.alertService.presentToast('Internal server error.');
          } else {
            this.alertService.presentToast('Failed to connect to the internet, please try again.');
          }
        });
      }
    }, error => {
      if (error.status === 500) {
        this.alertService.presentToast('Internal server error.');
      } else {
        this.alertService.presentToast('Failed to connect to the internet, please try again.');
      }
    });
  }

  // Edit Account
  editAccount() {
    this.modalCtrl.create({
      component: EditAccountPage,
      componentProps: {
        user_id: this.user_id,
        first_name: this.first_name,
        last_name: this.last_name,
        username: this.username,
        email: this.email,
        phone: this.phone
      }
    }).then(modal => {
      modal.present();
      return modal.onDidDismiss();
    }).then(resultData => {
      // Load new user data
      if (resultData.role === 'update') {
        const user = resultData.data.user;
        this.first_name = user.first_name;
        this.last_name = user.last_name;
        this.email = user.email;
        this.phone = '+62' + user.phone;
        this.username = user.username;
        this.alertService.presentToast('Successfully update user account.');
      }
    });
  }

  // Change Password
  changePassword() {
    this.modalCtrl.create({
      component: ChangePasswordPage,
      componentProps: { user_id: this.user_id }
    }).then(modal => {
      modal.present();
    });
  }

  // Logout
  async logout() {
    const alert = await this.alertCtrl.create({
      header: 'Confirmation',
      message: 'Are you sure you want to logout?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Logout',
          handler: () => {
            // Loading Controller
            this.loadingCtrl.create({
              spinner: 'crescent'
            }).then(loading => {
              loading.present();
              // Logout service post
              this.authService.logout().subscribe(() => {
                loading.dismiss();
                this.navCtrl.navigateRoot('/login');
              },
              error => {
                loading.dismiss();
                if (error.status === 500) {
                  this.alertService.presentToast('Internal server error.');
                } else {
                  this.alertService.presentToast('Failed to connect to the internet, please try again.');
                }
              });
            });
          }
        }
      ]
    });
    await alert.present();
  }

  ngOnDestroy() {
    if (this.authService) { this.authSubs.unsubscribe(); }
    if (this.barberSubs) { this.barberSubs.unsubscribe(); }
  }
}
