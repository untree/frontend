import { Subscription } from 'rxjs';
import { AlertService } from './../../../../services/alert/alert.service';
import { UserService } from './../../../../services/user/user.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ModalController, AlertController, LoadingController } from '@ionic/angular';
import { Component, OnInit, Input, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-edit-account',
  templateUrl: './edit-account.page.html',
  styleUrls: ['./edit-account.page.scss'],
})
export class EditAccountPage implements OnInit, OnDestroy {
  @Input() user_id: number;
  @Input() first_name: string;
  @Input() last_name: string;
  @Input() username: string;
  @Input() email: string;
  @Input() phone: string;
  form: FormGroup;
  userSubs: Subscription;

  constructor(
    private modalCtrl: ModalController,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private userService: UserService,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    // Edit Account Form
    this.form = new FormGroup({
      first_name: new FormControl(this.first_name, {
        updateOn: 'blur',
        validators: [Validators.required, Validators.minLength(3), Validators.maxLength(50)]
      }),
      last_name: new FormControl(this.last_name, {
        updateOn: 'blur',
        validators: [Validators.required, Validators.minLength(3), Validators.maxLength(50)]
      }),
      email: new FormControl(this.email, {
        updateOn: 'blur',
        validators: [Validators.required, Validators.minLength(3), Validators.maxLength(50), Validators.email]
      }),
      phone: new FormControl(this.phone.substr(3), {
        updateOn: 'blur',
        validators: [Validators.required]
      }),
      username: new FormControl(this.username, {
        updateOn: 'blur',
        validators: [Validators.required, Validators.minLength(3), Validators.maxLength(50)]
      })
    });
  }

  // Edit account button
  async editAccount() {
    console.log(this.form.value);
    if (!this.form.valid) { return; }

    const alert = await this.alertCtrl.create({
      header: 'Confirmation',
      message: 'Are you sure you want to update your account?',
      buttons: [{
        text: 'Cancel',
        role: 'cancel',
        handler: () => { console.log('cancel'); }
      }, {
        text: 'Yes',
        handler: () => {
          this.loadingCtrl.create({
            spinner: 'crescent',
            keyboardClose: true
          }).then(loading => {
            loading.present();
            // Post edit account
            this.userSubs = this.userService.editAccount(
              this.user_id + '',
              this.form.value['first_name'],
              this.form.value['last_name'],
              this.form.value['email'],
              this.form.value['phone'],
              this.form.value['username'],
            ).subscribe(() => {
              loading.dismiss();
              this.modalCtrl.dismiss({user: {
                first_name: this.form.value['first_name'],
                last_name: this.form.value['last_name'],
                email: this.form.value['email'],
                phone: this.form.value['phone'],
                username: this.form.value['username'],
              }}, 'update');
            }, error => {
              loading.dismiss();
              console.log(error);
              if (error.error.message.email) {
                this.alertService.presentToast('The email has already been used.');
              } else if (error.error.message.username) {
                this.alertService.presentToast('The username has already been used.');
              } else if (error.status === 500) {
                this.alertService.presentToast('Internal server error.');
              } else {
                this.alertService.presentToast('Failed to connect to the internet, please try again.');
              }
            });
          });
        }
      }]
    });
    await alert.present();
  }

  // Cancel edit account
  onCancel() {
    this.modalCtrl.dismiss(null, 'cancel');
  }

  ngOnDestroy() {
    if (this.userSubs) { this.userSubs.unsubscribe(); }
  }
}
