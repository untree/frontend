import { AuthService } from './../../services/auth/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})
export class TabsPage implements OnInit {
  role: string;

  constructor(
    private authService: AuthService
    ) { }

  ngOnInit() {
    this.authService.user().subscribe(
      user => {
        this.role = user.role;
    });
  }
}
