import { AlertService } from './../../services/alert/alert.service';
import { AuthService } from './../../services/auth/auth.service';
import { LoadingController, NavController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss']
})
export class LoginPage implements OnInit {
  form: FormGroup;

  constructor(
    private authService: AuthService,
    private alertService: AlertService,
    private loadingCtrl: LoadingController,
    private router: Router,
    private navCtrl: NavController
  ) {}

  ngOnInit() {
    this.authService.getToken().then(() => {
      if (this.authService.isLoggedIn) {
        this.navCtrl.navigateRoot('/tabs');
      }
    });
    
    this.form = new FormGroup({
      username: new FormControl(null),
      password: new FormControl(null)
    });
  }

  // Nav to Register
  register() {
    this.router.navigateByUrl('/register');
  }

  // Login
  login() {
    this.loadingCtrl.create({
        keyboardClose: true,
      }).then(loadingEl => {
        loadingEl.present();
        this.authService.login(this.form.value.username, this.form.value.password).subscribe(
          data => {
            loadingEl.dismiss();
            this.form.reset();
            if (data.role === 'Customer' || data.role === 'Admin') {
              this.router.navigateByUrl('/tabs/tabs/home');
            } else if (data.role === 'Barbershop') {
              this.router.navigateByUrl('/tabs/tabs/orders');
            }
          },
          error => {
            loadingEl.dismiss();
            if (error.status === 401 || error.status === 422) {
              this.alertService.presentToast('Username or password is invalid');
            } else if (error.status === 500) {
              this.alertService.presentToast('Internal server error.');
            } else {
              this.alertService.presentToast('Failed to connect to the internet, please try again.');
            }
          }
        );
    });
  }

  forgotPassword() {
    this.router.navigateByUrl('/forgot-password');
  }
}
