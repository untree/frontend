import { Router } from '@angular/router';
import { AuthService } from './../../services/auth/auth.service';
import { AlertService } from './../../services/alert/alert.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LoadingController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss']
})
export class RegisterPage implements OnInit {
  form: FormGroup;

  constructor(
    private authService: AuthService,
    private alertService: AlertService,
    private router: Router,
    private loadingCtrl: LoadingController
  ) {}

  ngOnInit() {
    this.form = new FormGroup({
      first_name: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required, Validators.minLength(3), Validators.maxLength(50)]
      }),
      last_name: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required, Validators.minLength(3), Validators.maxLength(50)]
      }),
      username: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required, Validators.minLength(3), Validators.maxLength(50)]
      }),
      email: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required, Validators.minLength(3), Validators.maxLength(50), Validators.email]
      }),
      phone: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required, Validators.minLength(12)]
      }),
      password: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required, Validators.minLength(6), Validators.maxLength(16)]
      })
    });
  }

  // Nav to Login
  login() {
    this.router.navigateByUrl('/login');
  }

  // Register
  register() {
    if (!this.form.valid) { return; }

    this.loadingCtrl.create({
        keyboardClose: true,
        message: 'Please wait...',
      }).then(loadingEl => {
        loadingEl.present();

        this.authService.register(
          this.form.value.first_name,
          this.form.value.last_name,
          this.form.value.username,
          this.form.value.email,
          this.form.value.phone,
          this.form.value.password
        ).subscribe(() => {
            loadingEl.dismiss();
            this.alertService.presentToast('Register successfull. Please login.');
            this.login();
          }, error => {
            loadingEl.dismiss();
            console.log(error);
            if (error.error.message.username) {
              this.alertService.presentToast('The username has already been used.');
            } else if (error.error.message.email) {
              this.alertService.presentToast('The email has already been used.');
            } else if (error.status === 500) {
              this.alertService.presentToast('Internal server error.');
            } else {
              this.alertService.presentToast('Failed to connect to the internet, please try again.');
            }
          }
        );
    });
  }
}
