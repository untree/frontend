import { AlertController, LoadingController } from '@ionic/angular';
import { AlertService } from './../../services/alert/alert.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.page.html',
  styleUrls: ['./forgot-password.page.scss'],
})
export class ForgotPasswordPage implements OnInit {
  form: FormGroup;

  constructor(
    private router: Router,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private userService: UserService,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    this.form = new FormGroup({
      email: new FormControl(null, { validators: Validators.required })
    });
  }

  async forgotPassword() {
    this.loadingCtrl.create({
      spinner: 'crescent',
      keyboardClose: true
    }).then(loading => {
      loading.present();
      this.userService.forgotPassword(this.form.value.email).subscribe(() => {
        loading.dismiss();
        this.alertCtrl.create({
          message: 'Please kindly check your email for your new password information.',
          buttons: [{
            text: 'OK',
            handler: () => {
              this.router.navigateByUrl('/login');
            }
          }]
        }).then(alert => alert.present());
      }, error => {
        loading.dismiss();
        if (error.error.status === 'This email is not registered.') {
          this.alertService.presentToast('This email is not registered.');
        } else if (error.status === 500) {
          this.alertService.presentToast('Internal server error.');
        } else {
          this.alertService.presentToast('Failed to connect to the internet, please try again.');
        }
      });
    });
  }
}
